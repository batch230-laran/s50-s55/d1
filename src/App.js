// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/ // s53 to comment
import { BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; //s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView'; // s55

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'; 
import Error from './pages/Error'; 

import AdminDashboard from './pages/AdminDashboard'

// s54 Additional Example
import Settings from './pages/Settings'; 

// s54
import { UserProvider } from './UserContext';

// s55
import { useState, useEffect, useContext } from 'react';

function App() {


  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  

  return (
    /* React Fragments allows us to return multiple elements*/

    // 2 - provide/share the state to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/admin" element={<AdminDashboard />} />

            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/courses/:courseId" element={<CourseView />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} /> 
            <Route exact path="/settings" element={<Settings />} /> 
            <Route exact path="*" element={<Error />} /> 
          </Routes>
        </Container>
      </ Router>
    </ UserProvider>
    
  );
}

export default App;
