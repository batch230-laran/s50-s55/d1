import { useState, useEffect, useContext } from "react";

import { Navigate, useNavigate } from "react-router-dom";
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Register(){
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    // create state hooks to store the values of the input fields
    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // create a state to detemine whether the submit button is enabled or not.
    const [isActive, setIsActive] = useState(false);

    /*
        Two Way Binding
            - is done so that we can assure that we can save the input into our states as we type into the input elements. This is so we dont't have to save it just before submit.

            e.target = current element where the event happened.
            e.target.value = current value of thje element where the event happened.
    */

    // Check if the values are successfully binding
    console.log(fName);
    console.log(lName);
    console.log(email);
    console.log(mobileNo);
    console.log(password1);
    console.log(password2);

    useEffect(() =>{

        // Enable the submit button if:
            // All the fields are populated.
            // both passwords match.

        if((fName !== '' && lName !=='' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])

    // Function to simulate user registration
    function registerUser(e){
        // Prevents page loading/ redirection via form submission.
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{

                fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: email,
                        password: password1,
                        mobileNumber: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        });

                        // Clear input fields
                        setFName('');
                        setLName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        // Allow us to redirect the user to the login page after account registration
                        navigate("/login");
                    }
                    else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });

                    }
                })


            }
        })

        

        // Notify user for registration
        // alert("Thank you for registering!");

    }

    return(
        // (user.id !== null)
        // ?
        //  <Navigate to="/courses" />
        // :
        <>
          
           
        </>
    )
}
