/*
  option 1 preparations:
  - npx create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap

  option 2 preparations
  - npm install -g create-react-app
  - create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap
*/

/*
  Files to delete:
  - App.test.js
  - index.css
  - logo.svg
  - reportWebVitals.js
*/

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

/* 
   remove:
  // import './index.css';
  // remove:
  // import reportWebVitals from './reportWebVitals';
*/ 

// We need to create a root to display React components 
  // Reference: https://beta.reactjs.org/reference/react-dom/client/createRoot
// You may check index.html of public folder to identify who is 'root'
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*
  const name = 'John Smith';
  const user = {
    firstName: 'Jane',
    lastName: 'Smith'
  }

  function formatName(parameterUser){
    return parameterUser.firstName + ' ' + parameterUser.lastName;
  }

  const element = <h1>Hello, {formatName(user)}</h1>

  // We need to create a root to display React components 
    // Reference: https://beta.reactjs.org/reference/react-dom/client/createRoot
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(element);
*/


